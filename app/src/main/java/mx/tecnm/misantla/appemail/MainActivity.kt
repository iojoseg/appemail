package mx.tecnm.misantla.appemail

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnEnviar.setOnClickListener {

            val correo = edtEmail.text.toString()
            val asunto = edtAsunto.text.toString()
            val mensaje = edtMensaje.text.toString()


            val TO = arrayOf(correo)
            val CC = arrayOf("")

            val emailIntent = Intent(Intent.ACTION_SEND)
            emailIntent.data = Uri.parse("mailto:")
            emailIntent.type = "text/plain"
            emailIntent.putExtra(Intent.EXTRA_EMAIL, TO)
            emailIntent.putExtra(Intent.EXTRA_CC, CC)

            emailIntent.putExtra(Intent.EXTRA_SUBJECT,asunto)
            emailIntent.putExtra(Intent.EXTRA_TEXT,mensaje)

            try{
                startActivity(Intent.createChooser(emailIntent,"Enviar Email"))
                finish()
            }catch (ex: android.content.ActivityNotFoundException){
                Toast.makeText(this, "No tiene un cliente de email",Toast.LENGTH_LONG).show()
            }

        }


    }
}